export const buildQueryParams = params => {
  const query = Object.keys(params).reduce(
    (carry, current) => `${current}=${params[current]}&${carry}`,
    ''
  );

  return query.replace(/&$/, '');
};
