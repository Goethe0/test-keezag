import React, { useEffect } from 'react';

import { getApiView } from "../../modules/appReducer";
import {push} from "connected-react-router";
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Header from "../../components/Header";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  title: {
    fontWeight: 700
  },
  imgCol: {
    padding: theme.spacing(2),
    textAlign: 'center',
  },
  img: {
    maxWidth: '90%'
  }
}));

const details = (props) => {
  const classes = useStyles();
  let id = 0;

  const { view } = props.app;

  useEffect(() => {
    if (!props.app.user.name) {
      return props.goToSignIn();
    }

    id = props.match.params.id;
    props.getApiView(id);
  }, []);

  return (
    <div className={classes.root}>
      <Header {...props} />
      {view && (
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <Paper className={[classes.paper, classes.title]}>{view.name}</Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.imgCol}>
              <img
                src={`${view.thumbnail.path}.${view.thumbnail.extension}`} alt={view.name}
                className={classes.img}
              />
            </Paper>
          </Grid>
          {view.description &&
            <Grid item xs={12}>
              <Paper className={classes.paper}>{view.description}</Paper>
            </Grid>
          }
          <Grid item xs={6}>
            <Paper className={classes.paper}>Séries: {view.series.available}</Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper className={classes.paper}>Comics: {view.comics.available}</Paper>
          </Grid>
          {view.series.items.map(item => (
            <Grid item xs={3}>
              <Paper className={classes.paper}>{item.name}</Paper>
            </Grid>
          ))}
        </Grid>
      )}
    </div>
  )
};

const mapStateToProps = ({ counter, appReducer }) => ({
  app: appReducer,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getApiView,
      goToSignIn: () => push('/'),
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(details)
