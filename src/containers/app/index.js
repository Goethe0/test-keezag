import React from 'react'
import { Route } from 'react-router-dom'
import Home from '../home'
import About from '../about'
import SignIn from '../signin/signin.js';
import Details from '../details'

const App = () => (
  <div>
    <main>
      <Route exact path="/" component={SignIn} />
      <Route exact path="/home" component={Home} />
      <Route exact path="/about-us" component={About} />
      <Route path="/view/:id" component={Details} />
    </main>
  </div>
);

export default App
