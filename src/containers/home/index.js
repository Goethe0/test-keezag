import React, { useEffect } from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {
  getApiList
} from "../../modules/appReducer";

import Button from '@material-ui/core/Button';
import ListItems from "../../components/ListItems";
import Header from "../../components/Header";

const Home = props => {

  const { list } = props.app;

  useEffect(() => {
    if (!props.app.user.name) {
      return props.goToSignIn();
    }
    props.getApiList();
  }, []);

  return (
    <div>
      <Header {...props} />
      <h3 className="text-center">Lista de Super Heróis</h3>
      <Button variant="contained" color="primary" onClick={props.getApiList} className="hidden">
        Atualizar lista
      </Button>

      {list.results.length > 0 &&
        <ListItems
          data={list}
          goToDetail={props.goToDetail}
          getApiList={props.getApiList}
        />
      }
    </div>
  );
};

const mapStateToProps = ({ counter, appReducer }) => ({
  app: appReducer,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getApiList,
      goToSignIn: () => push('/'),
      goToAbout: () => push('/about-us'),
      goToDetail: (id) => push(`/view/${id}`)
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
