import React from 'react'

const About = () => (
  <div>
    <h1>Sobre</h1>
    <p>LinkedIn:
      <a
        href="https://www.linkedin.com/in/goethe0/"
        target="_blank"
        rel="noopener noreferrer"
      >
        Diego Araujo
      </a>
    </p>
  </div>
);

export default About
