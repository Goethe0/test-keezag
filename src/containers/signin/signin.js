import React, { useState, useEffect } from 'react';

import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'


import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import { authUser, logout } from "../../modules/appReducer";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Teste React - '}
      <Link color="inherit" href="http://criacaoapp.com.br/">
        Diego Araujo
      </Link>
      {'.'}
    </Typography>
  );
}

function SignIn(props) {
  const classes = useStyles();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const submit = () => {
    props.authUser(email, password);
  };

  useEffect(() => {
    props.logout();
  }, []);

  useEffect(() => {
    if (props.app.user.name) {
      props.goToHome();
    }
  }, [props.app]);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email ou Usuário"
            name="email"
            autoComplete="email"
            value={email}
            onChange={el => setEmail(el.target.value)}
            autoFocus
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Senha"
            type="password"
            id="password"
            value={password}
            autoComplete="current-password"
            onChange={el => setPassword(el.target.value)}
          />
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={(!email || !password)}
            onClick={submit}
          >
            Login
          </Button>
          {props.app.loginMessage &&
            <Alert severity="error">{props.app.loginMessage}</Alert>
          }
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const mapStateToProps = ({ counter, appReducer }) => ({
  app: appReducer,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      authUser,
      logout,
      goToHome: () => push('/home')
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignIn)
