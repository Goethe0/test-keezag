import md5 from 'md5';

export const ROUTE_API_LOGIN = 'assets/users.json';
export const ROUTE_API_LIST = 'http://gateway.marvel.com/v1/public/characters';
export const ROUTE_API_VIEW = 'http://gateway.marvel.com/v1/public/characters/:id';

export const API_PUB_KEY = "213d6abb547390fcf7a46d94de76fbba";
export const API_PV_KEY = "bf38d5bc1caaa582431e5d35303798e896b90214";

export const authUserCall = () => {
  const headers = { 'Content-Type': 'application/json' };
  const request = new Request(
    ROUTE_API_LOGIN,
    {
      method: 'GET',
      headers: new Headers(headers)
    });

  return fetch(request).then(response =>
    response.json().then(body => ({ status: response.status, body }))
  );
};

export const getApiListCall = (offset) => {
  const headers = { 'Content-Type': 'application/json' };

  const ts = Math.floor(new Date().getTime() / 1000);
  const hash = md5(ts + API_PV_KEY + API_PUB_KEY);
  const url = `${ROUTE_API_LIST}?ts=${ts}&apikey=${API_PUB_KEY}&hash=${hash}&orderBy=-modified&offset=${offset}&limit=6`;

  const request = new Request(
    url,
    {
      method: 'GET',
      headers: new Headers(headers),

    });

  return fetch(request).then(response =>
    response.json().then(body => ({ status: response.status, body }))
  );
};

export const getApiViewCall = (id) => {
  const headers = { 'Content-Type': 'application/json' };

  const ts = Math.floor(new Date().getTime() / 1000);
  const hash = md5(ts + API_PV_KEY + API_PUB_KEY);
  const url = `${ROUTE_API_VIEW.replace(":id", id)}?ts=${ts}&apikey=${API_PUB_KEY}&hash=${hash}`;

  const request = new Request(
    url,
    {
      method: 'GET',
      headers: new Headers(headers),

    });

  return fetch(request).then(response =>
    response.json().then(body => ({ status: response.status, body }))
  );
};
