import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const Header = (props) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const { user } = props.app;

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <React.Fragment>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
        MENU
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem>
          <Link to="/home">
            Lista de Heróis
          </Link>
        </MenuItem>
        <MenuItem>
          <a
            href="https://www.linkedin.com/in/goethe0/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Sobre
          </a>
        </MenuItem>
        <MenuItem>
          <Link to="/">
            Logout
          </Link>
        </MenuItem>
      </Menu>
      <span>
        - {user.name}
      </span>
    </React.Fragment>
  );
};

export default Header;
