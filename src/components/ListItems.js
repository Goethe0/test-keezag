import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: '100%',
    margin: 10,
    // height: 450,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  buttonDiv: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '2rem'
  },
  tileBar: {
    cursor: 'pointer'
  }
}));

export default function ListItems({data, goToDetail, getApiList}) {
  const classes = useStyles();

  const nextPage = () => {
    const offset = data.offset + 6;
    getApiList(offset)
  };

  const previousPage = () => {
    const offset = data.offset - 6 < 0 ? 0 : data.offset - 6;
    getApiList(offset)
  };

  const page = data.offset / 6 + 1;

  return (
    <div className={classes.root}>
      <GridList cellHeight={180} className={classes.gridList}>
        <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
          <ListSubheader component="div">Página {page}</ListSubheader>
        </GridListTile>
        {data.results.map(tile => (
          <GridListTile key={tile.id}>
            <img src={`${tile.thumbnail.path}.${tile.thumbnail.extension}`} alt={tile.name} />
            <GridListTileBar
              className={classes.tileBar}
              title={tile.name}
              subtitle={<span>{tile.description}</span>}
              actionIcon={
                <IconButton aria-label={`info about ${tile.title}`} className={classes.icon}>
                  <InfoIcon />
                </IconButton>
              }
              onClick={() => goToDetail(tile.id)}
            />
          </GridListTile>
        ))}
      </GridList>
      <br />
      <div className={classes.buttonDiv}>
        <p>

        </p>
        {data.offset > 0 && (
          <Button variant="contained" color="primary" onClick={previousPage}>
            Página Anterior
          </Button>
        )}
        <Button variant="contained" color="primary" onClick={nextPage}>
          Próxima Página
        </Button>
      </div>
    </div>
  );
}
