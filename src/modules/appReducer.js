import {authUserCall, getApiListCall, getApiViewCall} from "../api/appCalls";

export const API_LOGIN = 'API_LOGIN';
export const API_LOGOUT = 'API_LOGOUT';
export const API_LIST = 'API_LIST';
export const API_VIEW = 'API_VIEW';

const initialState = {
  user: {},
  loginMessage: '',
  list: {
    offset: 0,
    total: 0,
    results: []
  },
  view: {
    id: 0,
    name: '',
    thumbnail: {},
    comics: {
      available: 0,
      items: []
    },
    series: {
      available: 0,
      items: []
    },
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case API_LOGIN:
      return {
        ...state,
        user: action.payload.user,
        loginMessage: action.payload.message
      };
    case API_LOGOUT:
      return {
        ...state,
        user: '',
        loginMessage: ''
      };
    case API_LIST:
      return {
        ...state,
        list: action.payload
      };
    case API_VIEW:
      return {
        ...state,
        view: action.payload
      };
    default:
      return state;
  }
};


export const authUser = (username, password) => dispatch => {
  authUserCall().then(response => {
    let user = response.body.filter(user =>
      ((user.user === username || user.email === username)
        && user.password.toString() === password)
    );
    let message = '';

    if (!user || user.length < 1) {
      user = {};
      message = 'Dados Errados.'
    } else {
      user = user[0];
    }

    const payload = { user, message };
    dispatch({ type: API_LOGIN, payload });
  });
};

export const logout = () => dispatch => dispatch({ type: API_LOGOUT });


export const getApiList = (offset = 0) => dispatch => {
  getApiListCall(offset).then(response => {
    dispatch({ type: API_LIST, payload: response.body.data });
  });
};

export const getApiView = (id) => dispatch => {
  getApiViewCall(id).then(response => {
    dispatch({ type: API_VIEW, payload: response.body.data.results[0] });
  });
};
