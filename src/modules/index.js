import { combineReducers } from 'redux'
import counter from './counter'
import appReducer from './appReducer'

export default combineReducers({
  counter,
  appReducer
})
